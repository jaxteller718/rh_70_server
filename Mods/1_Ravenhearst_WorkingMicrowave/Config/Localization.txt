﻿Key,Source,Context,English
workingMicrowaveRH,blocks,Block,Microwave (Powered)
workingMicrowaveRHDesc,blocks,Block,"This microwave is powered already and can be used to heat up bowls of canned food to gain the maximum benefits from them."
drinkBowlEmptyRH,items,Item,Bowl (Empty)
drinkBowlEmptyRHDesc,items,Item,Use this empty bowl to heat up foods in the microwave.
foodSoupChickenRH,items,Food,Bowl of Chicken Soup
foodSoupChickenRHDesc,items,Food,Using your saved canned food with bowls in the microwave can give you additional benefits. Nothing beats a warm bowl of food.
foodBowlPastaRH,items,Food,Bowl of Pasta
foodBowlPastaRHDesc,items,Food,Using your saved canned food with bowls in the microwave can give you additional benefits. Nothing beats a warm bowl of food.
foodBowlChiliRH,items,Food,Bowl of Chili
foodBowlChiliRHDesc,items,Food,Using your saved canned food with bowls in the microwave can give you additional benefits. Nothing beats a warm bowl of food.
foodBeefStewRH,items,Food,Bowl of Beef Stew
foodBeefStewRHDesc,items,Food,Using your saved canned food with bowls in the microwave can give you additional benefits. Nothing beats a warm bowl of food.
foodLambSoupRH,items,Food,Bowl of Lamb Soup
foodLambSoupRHDesc,items,Food,Using your saved canned food with bowls in the microwave can give you additional benefits. Nothing beats a warm bowl of food.
foodTunaSandwichRH,items,Food,Tuna Sandwich
foodTunaSandwichRHDesc,items,Food,Making a delicious tuna sandwich will give you some extra benefits.
microwaveTipRH,Journal,EnChanged,The microwave can be placed and used to heat canned food recipes. Canned food recipes offer slightly more benefits to them than if you eat them in singles so save those cans when you can. Bowls can be found in loot and are used with the canned foods.,,
microwaveTipRH_title,Journal,KeyTrunk,Microwave