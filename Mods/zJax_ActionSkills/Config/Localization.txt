﻿Key,Source,Context,English
perkASMiningToolsDesc,progression,Perk,"+1% Block Damage and Resource Gain\n+0.3% Stamina Reduction\nper level"
perkASConstructionToolsDesc,progression,Perk,"+1% Block Damage and Resource Gain\n+0.3% Stamina Reduction\nper level"
perkASBluntDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Baseball Bat at level 50"
perkASSledgehammersDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Iron Sledgehammer at level 50"
perkASBladedDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Hunting Knife at level 50"
perkASElectricDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Robotic Sledge at level 50"
perkASJavelinDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Iron Spear at level 50"
perkASArcheryDesc,progression,Perk,"+0.3% Damage\n+0.5% Accuracy and Reload Speed\nper level\nUnlocks Wooden Bow and Iron Crossbow at level 50"
perkASPistolsDesc,progression,Perk,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level\nUnlocks Pistol at level 50"
perkASShotgunsDesc,progression,Perk,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level\nUnlocks Double Barrel Shotgun at level 50"
perkASRiflesDesc,progression,Perk,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level\nUnlocks Hunting Rifle at level 50"
perkASAutomaticsDesc,progression,Perk,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level\nUnlocks AK-47 at level 50"
perkASBrawlerDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Iron Knuckles at level 50""
perkASTurretDesc,progression,Perk,"+0.3% Damage and Stamina Reduction\nper level\nUnlocks Robotic Sledge at level 50"
attactionskills,progression,Attribute,Action Skills
attActionSkillsDesc,progression,Attribute,Action Skills level as you use them.
attActionSkillsName,progression,Attribute,Attribute: Action Skills
ttMToolsLevelUp,UI,Tooltip,"Your Skill In Mining Tools Has Increased!",,,,,
ttArcheryLevelUp,UI,Tooltip,"Your Skill In Archery Has Increased!",,,,,
ttAutomaticsLevelUp,UI,Tooltip,"Your Skill In Automatics Has Increased!",,,,,
ttBluntLevelUp,UI,Tooltip,"Your Skill In Blunt Weapons Has Increased!",,,,,
ttBladedLevelUp,UI,Tooltip,"Your Skill In Blades Has Increased!",,,,,
ttPistolLevelUp,UI,Tooltip,"Your Skill In Pistols Has Increased!",,,,,
ttShotgunLevelUp,UI,Tooltip,"Your Skill In Shotguns Has Increased!",,,,,
ttCToolsLevelUp,UI,Tooltip,"Your Skill In Construction Tools Has Increased!",,,,,
ttRiflesLevelUp,UI,Tooltip,"Your Skill In Rifles Has Increased!",,,,,
ttElectricLevelUp,UI,Tooltip,"Your Skill In Batons Has Increased!",,,,,
ttTurretLevelUp,UI,Tooltip,"Your Skill In Turrets Has Increased!",,,,,
ttJavelinLevelUp,UI,Tooltip,"Your Skill In Spears Has Increased!",,,,,
ttBrawlerLevelUp,UI,Tooltip,"Your Skill In Brawler Has Increased!",,,,,
ttBluntLevelUp,UI,Tooltip,"Your Skill In Blunt Weapons Has Increased!",,,,,
