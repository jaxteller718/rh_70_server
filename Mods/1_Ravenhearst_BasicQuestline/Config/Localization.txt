﻿Key,English
quest_BasicSurvival1a_subtitle,Gather Logs and Wood,,,,,
quest_BasicSurvival1a_description,Wood is an important part of creating items and building materials. To gather wood attack a tree. You will receive logs. These wood logs can be crafted into wood. Wood has many purposes including the ability to make it into sticks and planks. You can also gather sticks by attacking small stick bushes in the world.,,,,,
quest_BasicSurvival2a_subtitle,Craft A Stone Pickaxe,,,,,
quest_BasicSurvival2a_description,A stone axe is great for breaking down wood doors and trees but it will not get the job done when you need to break something made of stone or metal. Craft a stone pickaxe. Using it you can mine basic starter resources from boulders. As you gain access to better tiered pickaxes you will find you can mine for better resources.,,,,,
quest_BasicSurvival2b_subtitle,Collect Some Ore,,,,,
quest_BasicSurvival2b_description,Use your stone pickaxe and find a boulder. Mining this boulder will give you some small traces of ore. You may have to hit 2. Other pickaxes will give you more precious metal. Underground mining will yield resources like limestone for cement.,,,,,
quest_BasicSurvival6a_subtitle,Find Storage,,,,,
quest_BasicSurvival6a_description,"The trash you see in the streets is not only useful for things like twine, but they can also be used to store your items. For every 3 trash bags you collect you can make a bigger trash bag and store stuff inside. Fine 3 bags and collect them by destroying them


