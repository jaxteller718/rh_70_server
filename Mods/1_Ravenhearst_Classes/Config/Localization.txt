﻿Key,Source,Context,English
qStarterItems,items,Quest - Note,Starter Kit,,,,,
qStarterItemsDesc,items,Quest - Note,Note from Jax\n\nThe rest of the club is either scattered or dead. You and I are all that is left. I had to head north. I have left you some stuff to help you get to us. Use your special skill wisely because it is all you have in this fucked up world.,,,,,

noteStarterClassSelection,items,Quest - Note,Class Selection,,,,,
noteStarterClassSelectionDesc,items,Quest - Note,Use this paper to select your weapons class. Classes do not lock you out of progressing but they do offer unique recipes for weapons you can craft in each category when you complete the line.,,,,,

starterkitName,Quest,Quest Info,Welcome to Ravenhearst,,,,,

starterkit,Quest,Quest Info,"Read This First",,,,,
starterkit_offer,Quest,Quest Info,"The week before the outbreak began, the hospitals of Ravenhearst were bombarded with overdoses thanks to a new designer drug manufactured by the Makasin Cartel. Their towers were the first to be locked down. The Outlaw Bikers MC, screwed over by the Makasin, laid siege to their headquarters. \n\nThe government called in the military to take control and failed, all hell broke loose.",,,,,
starterkit_description,Quest,Quest Info,"Ravenhearst Has Fallen",,,,,
starterkit_subtitle,Quest,Quest Info,The Outbreak Begins,,,,,

qPistolClass,items,Quest - Note,Pistol Class,,,,,
qPistolClassDesc,items,Quest - Note,Read this book to begin your training in pistols. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qShotgunClass,items,Quest - Note,Shotgun Class,,,,,
qShotgunClassDesc,items,Quest - Note,Read this book to begin your training in shotguns. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qRifleClass,items,Quest - Note,Rifle Class,,,,,
qRifleClassDesc,items,Quest - Note,Read this book to begin your training in rifles. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qAutomaticsClass,items,Quest - Note,Automatics Class,,,,,
qAutomaticsClassDesc,items,Quest - Note,Read this book to begin your training in automatic weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qArcheryClass,items,Quest - Note,Archer Class,,,,,
qArcheryClassDesc,items,Quest - Note,Read this book to begin your training in bows and crossbows. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qBluntClass,items,Quest - Note,Blunt Class,,,,,
qBluntClassDesc,items,Quest - Note,Read this book to begin your training in blunt weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qBladeClass,items,Quest - Note,Blade Class,,,,,
qBladeClassDesc,items,Quest - Note,Read this book to begin your training in bladed weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,


q_PistolClass,Quest,Quest Info,Pistol Training,,,,,

q_PistolClass0,Quest,Quest Info,"Using Your Pistol",,,,,
q_PistolClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_PistolClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_PistolClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_PistolClass1,Quest,Quest Info,"Using Your Pistol",,,,,
q_PistolClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_PistolClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_PistolClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_PistolClass2,Quest,Quest Info,"Using Your Pistol",,,,,
q_PistolClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_PistolClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_PistolClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_ShotgunClass,Quest,Quest Info,Shotgun Training,,,,,

q_ShotgunClass0,Quest,Quest Info,"Using Your Shotgun",,,,,
q_ShotgunClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ShotgunClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ShotgunClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ShotgunClass1,Quest,Quest Info,"Using Your Shotgun",,,,,
q_ShotgunClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ShotgunClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ShotgunClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ShotgunClass2,Quest,Quest Info,"Using Your Shotgun",,,,,
q_ShotgunClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ShotgunClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ShotgunClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_RifleClass,Quest,Quest Info,Rifle Training,,,,,

q_RifleClass0,Quest,Quest Info,"Using Your Rifle",,,,,
q_RifleClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_RifleClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_RifleClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_RifleClass1,Quest,Quest Info,"Using Your Rifle",,,,,
q_RifleClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_RifleClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_RifleClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_RifleClass2,Quest,Quest Info,"Using Your Rifle",,,,,
q_RifleClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_RifleClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_RifleClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_AutomaticsClass,Quest,Quest Info,Automatic Weapon Training,,,,,

q_AutomaticsClass0,Quest,Quest Info,"Using Your Automatic",,,,,
q_AutomaticsClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_AutomaticsClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_AutomaticsClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_AutomaticsClass1,Quest,Quest Info,"Using Your Automatic",,,,,
q_AutomaticsClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_AutomaticsClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_AutomaticsClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_AutomaticsClass2,Quest,Quest Info,"Using Your Automatic",,,,,
q_AutomaticsClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_AutomaticsClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_AutomaticsClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_ArcheryClass,Quest,Quest Info,Archery Training,,,,,

q_ArcheryClass0,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass1,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass2,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass3,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass4,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass4_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass4_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass4_subtitle,Quest,Quest Info,"Kill Zombies",,,,,




q_BluntClass,Quest,Quest Info,Blunt Weapons Training,,,,,

q_BluntClass0,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass1,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass2,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass3,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass4,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass4_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass4_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass4_subtitle,Quest,Quest Info,"Kill Zombies",,,,,




q_BladeClass,Quest,Quest Info,Bladed Weapons Training,,,,,

q_BladeClass0,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BladeClass1,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BladeClass2,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_BladeClass3,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



